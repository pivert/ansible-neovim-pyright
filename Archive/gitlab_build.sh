#!/usr/bin/env bash
set -e

if git status | grep "nothing to commit"
then
  TAG=$(git tag | tail -n1)
  NEWTAG=$(bc <<< "$TAG + 0.01")
  sed -E -i -e "s/^(LABEL org.opencontainers.image.version=).*/\1\"${NEWTAG}\"/" Dockerfile
  git commit -am "Updated image version to ${NEWTAG}"
  git push
  git tag $NEWTAG
  git push --tags
else
  echo "ERROR: Work to be commited"
  exit 1
fi
