FROM python:3-slim-bookworm AS base

ARG MAINTAINER="François Delpierre <docker@pivert.org>"
ARG VERSION="3.44"
ARG GIT_COMMIT=unspecified
ARG GIT_TAG=unspecified
ARG NODE_MAJOR=20
LABEL git_commit=${GIT_COMMIT}
LABEL maintainer="${MAINTAINER}"
LABEL org.opencontainers.image.title="seashell DevOps toolbox"
LABEL description="Workstation in a container"
LABEL org.opencontainers.image.url="https://gitlab.com/pivert/seashell/"
LABEL org.opencontainers.image.documentation="https://gitlab.com/pivert/seashell/"
LABEL org.opencontainers.image.version="${VERSION}"

ENV VERSION="${VERSION}"
ENV TZ=Etc/UTC

## Enable man
RUN sed -i '/path-exclude \/usr\/share\/man/d' /etc/dpkg/dpkg.cfg.d/docker \
&& sed -i '/path-exclude \/usr\/share\/groff/d' /etc/dpkg/dpkg.cfg.d/docker

# Install nodejs repositories
# auto-apt-proxy can be useful if you run an apt-cacher-ng in the network
RUN apt-get update \
  && apt-get install -y auto-apt-proxy ca-certificates curl gnupg wget \
  && mkdir -p /etc/apt/keyrings \
  && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key \
  | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg \
  && echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" \
  | tee /etc/apt/sources.list.d/nodesource.list \
  && apt-get update

# Enable Microsoft & Mongo repositories
RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /etc/apt/trusted.gpg.d/microsoft.gpg \
  && curl https://packages.microsoft.com/config/debian/11/prod.list | tee /etc/apt/sources.list.d/mssql-release.list
ENV ACCEPT_EULA=Y

# Enable GitHub and GitLab repos for CLIs
RUN mkdir -p -m 755 /etc/apt/keyrings \
  && out=$(mktemp) && wget -nv -O$out https://cli.github.com/packages/githubcli-archive-keyring.gpg \
  && cat $out | tee /etc/apt/keyrings/githubcli-archive-keyring.gpg > /dev/null \
  && chmod go+r /etc/apt/keyrings/githubcli-archive-keyring.gpg \
  && echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | tee /etc/apt/sources.list.d/github-cli.list > /dev/null
RUN curl -sSL "https://raw.githubusercontent.com/upciti/wakemeops/main/assets/install_repository" | bash

# Mongo Client
RUN curl -fsSL https://www.mongodb.org/static/pgp/server-8.0.asc | \
   gpg -o /usr/share/keyrings/mongodb-server-8.0.gpg --dearmor \
  && echo "deb [ signed-by=/usr/share/keyrings/mongodb-server-8.0.gpg ] http://repo.mongodb.org/apt/debian bookworm/mongodb-org/8.0 main" | tee /etc/apt/sources.list.d/mongodb-org-8.0.list

# Install packages (including up to date NodeJS)
RUN  apt-get install -y \
  aha \
  ash \
  apache2-utils \
  bash-completion \
  bind9-host \
  bc \
  bzip2 \
  bind9-dnsutils \
  command-not-found \
  cowsay \
  docker.io \
  dos2unix \
  fd-find \
  gh \
  glab \
  git \
  git-crypt \
  hyperfine \
  htop \
  iproute2 \
  iputils-ping \
  j2cli \
  jsonnet \
  jq \
  less \
  libssl-dev \
  libaio1 \
  libpq-dev \
  make \
  man \
  mariadb-client \
  minicom \
  netcat-openbsd \
  nmap \
  nodejs \
  pkg-config \
  postgresql-client-15 \
  procps \
  python3-venv \
  pv \
  pwgen \
  ripgrep \
  rsync \
  silversearcher-ag \
  sqlite3 \
  ssh-client \
  strace \
  subversion \
  sudo \
  telnet \
  tmux \
  tree \
  iputils-tracepath \
  unzip \
  whois \
  xz-utils \
  zip \
  && apt-file update \
  && update-command-not-found \
  && apt-get clean


RUN chmod 640 /etc/sudoers

# dockerfile language server for Neovim
RUN npm install -g --no-update-notifier npm \
  && npm install -g dockerfile-language-server-nodejs neovim \
  && npm cache clean --force \
  && rm -rf /user/.cache \
  && rm -rf /root/.npm

ENV HOME=/user

COPY README.md /


## Build
FROM base AS build


# Copy default user profile, ex/view/vimdiff shortcuts, and the /init.sh
# Required before nvim compile scripts because of the shortcuts scripts
COPY files/ /
WORKDIR /build

# Compile and install Neovim from source
RUN ./compile.sh

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile minimal --quiet -y --no-modify-path
ENV PATH="$PATH:/user/.cargo/bin"

RUN /bin/bash || true
# The Ansible collection is compressed (from 400MB to 40MB). Run /untaransiblecollection.sh to get it back.
# Pandas tests are compressed as well. Just there in case of.
RUN python -m pip install --no-cache-dir --upgrade pip \
   && pip install --no-cache-dir -r requirements.txt  \
   && SP=$(python -c 'import site;print(site.getsitepackages()[0])') \
   && tar -Jcf ${SP}/ansible_collections.tar.xz ${SP}/ansible_collections \
   && rm -rf ${SP}/ansible_collections 
   #&& tar -Jcf ${SP}/pandas/tests.tar.xz ${SP}/pandas/tests \
   #&& rm -rf ${SP}/pandas/tests
 
# P.S. several chmod are required in the home folder since :
# - We don't know yet the uid of the user (set at init.sh script)
# - A unique chmod on /user at the end would grow the image

# Neovim Plug & Coc
RUN sh -c 'curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim' \
  && chmod -R a=rwX /user/.local

# Take only the Plug part since Coc part will fail if Plug plugins are not installed first.
# This file will also be used when root starts nvim (so root won't have coc plugins)
RUN mkdir -p /usr/share/nvim \
  && sed '/call plug#end()/q' /user/.config/nvim/init.vim > /usr/share/nvim/sysinit.vim
RUN nvim -e -u /usr/share/nvim/sysinit.vim -i NONE -c "PlugInstall|q" -c "qa" \
   && echo "Plug Plugins Install: OK" \
   && chmod -R a=rwX /user/.vim \
   && chmod -R a=rwX /user/.local/state \
   && rm -rf /user/.cache

## NVIM
# Install node packages for Coc to work - Ignore errors if a plugin is not fully installed
# Info: The vue & dependancies adds 100MB to the compressed image.
RUN nvim -e -c \
  "CocInstall -sync coc-spell-checker coc-git coc-pyright coc-json coc-docker coc-sh\
  coc-yaml coc-html @yaegassy/coc-ansible coc-prettier @yaegassy/coc-volar coc-lua|q" \
  && npm cache clean --force \
  && chmod -R a=rwX /user/.config \
  && chmod -R a=rwX /user/.npm \
  && chmod -R a=rwX /user/.local/state/nvim

# Double check coc plugins installation
RUN test $(du -sm /user/.config/coc/extensions/node_modules/ | awk '{print $1}') -gt 200 \
  && test $(ls -d /user/.config/coc/extensions/node_modules/* | wc -l) -gt 8


## 
FROM base AS stage2
# Disable coc cSpell in yaml files (too many false positives)
# RUN tmp=$(mktemp) \
#   && jq '.[cSpell.enabledLanguageIds] |= del(.[] | select(. == "yml"))' ~/.config/nvim/coc-settings.json > ${tmp} \
#   && mv ${tmp} ~/.config/nvim/coc-settings.json

# COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local /usr/local
COPY --from=build /user /user
COPY --from=build /build /build
COPY --from=build /usr/bin/vim /usr/bin/vim
COPY --from=build /etc/profile.d /etc/profile.d
COPY --from=build /etc/bash.bashrc /etc/bash.bashrc

# Ensure root can also access Neovim Config
RUN sed -ie 's!^root:x:0:0:root:.*!root:x:0:0:root:/user:/bin/bash!' /etc/passwd

# Install OpenFaaS # Disabled here since it's in the "compile script above"
# RUN curl -sSL https://cli.openfaas.com | sh \
# && faas-cli completion --shell bash > /etc/bash_completion.d/faas-cli

## K8S
# Install oc & kubectl from OKD4
# Commented out because of the many vulnerabilities and disk usage - Replaced by kubectl deb package
# RUN wget https://mirror.openshift.com/pub/openshift-v4/clients/oc/latest/linux/oc.tar.gz -O - | tar -xz -C /usr/local/bin/

# Get bash completion for kubectl and oc
RUN /usr/local/bin/kubectl completion bash > /etc/bash_completion.d/kubectl \
  && chmod a+r /etc/bash_completion.d/kubectl 
#  && chmod a+r /etc/bash_completion.d/oc
# && /usr/local/bin/oc completion bash > /etc/bash_completion.d/oc \

# Install krew (kubectl plugins repository)
RUN mkdir krew && cd krew \
  && curl -sSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew-linux_amd64.tar.gz" \
  && tar -zxvf krew-linux_amd64.tar.gz \
  && ./krew-linux_amd64 install krew \
  && cd .. && rm -rf krew \
  && chmod -R a+rwX /user/.krew \
  && /user/.krew/bin/kubectl-krew update

# Install stern via krew
RUN /user/.krew/bin/kubectl-krew install stern \
  && chmod -R a+rwX /user/.krew

# Install kubectx & kuberns (very handful for switching ns or cluster)
RUN git clone -b master --single-branch --depth 1 https://github.com/ahmetb/kubectx /opt/kubectx \
  && ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx \
  && ln -s /opt/kubectx/kubens /usr/local/bin/kubens \
  && ln -s /opt/kubectx/kubectx /usr/local/bin/kctx \
  && ln -s /opt/kubectx/kubens /usr/local/bin/kns \
  && ln -s /opt/kubectx/completion/kube*.bash /etc/bash_completion.d/

# Default to bash for new users
RUN sed -ie 's!^SHELL=/bin/sh!SHELL=/bin/bash!' /etc/default/useradd
# Download and verify checksum of k9s
RUN /build/download_k9s.sh

FROM stage2 AS syft

RUN curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | sh -s -- -b /usr/local/bin
RUN syft packages / | gzip -9 > /sbom.txt.gz && chmod 666 /sbom.txt.gz && rm -rf /user/.cache

## Final
FROM stage2 AS final

COPY files/init.sh /init.sh
COPY --from=syft /sbom.txt.gz /sbom.txt.gz

WORKDIR /workdir
RUN chmod a+x /init.sh

ENTRYPOINT [ "/init.sh" ]
CMD [ "/bin/bash", "-l" ]
