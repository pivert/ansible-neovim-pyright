wget https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip
unzip aws-sam-cli-linux-x86_64.zip -d sam-installation
./sam-installation/install
sam --version
rm aws-sam-cli-linux-x86_64.zip
rm -rf ./sam-installation
# Just removing because it generates false positive in trivy
rm -rf /usr/local/aws-sam-cli/*/dist/samcli/lib/generated_sample_events
