#!/usr/bin/env bash

set -Eeuo pipefail

curl -sLO https://github.com/neovim/neovim/releases/download/stable/nvim-linux64.deb
dpkg -i nvim-linux64.deb
rm nvim-linux64.deb

# Set Neovim shortcuts
NVIM_PATH=/usr/bin
update-alternatives --install /usr/bin/vi neovim "${NVIM_PATH}/nvim" 110
update-alternatives --set neovim "${NVIM_PATH}/nvim"
update-alternatives --install /usr/bin/vim vim "${NVIM_PATH}/nvim" 110
update-alternatives --install /usr/bin/editor editor "${NVIM_PATH}/nvim" 1
