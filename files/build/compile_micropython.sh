#!/bin/bash

set -Eeuo pipefail

# This script installs software compiled from source
apt-get install -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen build-essential libffi-dev pkg-config
git clone -b stable --single-branch --depth 1 https://github.com/neovim/neovim.git

# Build and install micropython
git clone -b master --single-branch --depth 1 https://github.com/micropython/micropython.git 
cd micropython/ports/unix/
make submodules
make
make install
cd ../../../
rm -rf micropython

# Clean up build packages
apt-get purge -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config doxygen  build-essential libffi-dev pkg-config

apt-get autoremove -y
apt-get clean

rm -rf /root/.cache

