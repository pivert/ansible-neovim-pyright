#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#define KEY_SEARCH_LIMIT 16
#define MAX_NAME_LENGTH 64

typedef struct Context {
    char name[MAX_NAME_LENGTH];
    char namespace[MAX_NAME_LENGTH];
} Context;

int line_starts_with_alnum(const char *line) {
    return isalnum(*line);
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        return 1;
    }

    int fd = open(argv[1], O_RDONLY);
    if (fd == -1) {
        return 1;
    }

    struct stat st;
    if (fstat(fd, &st) == -1) {
        close(fd);
        return 1;
    }

    char *data = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (data == MAP_FAILED) {
        close(fd);
        return 1;
    }
    close(fd);

    char current_context[MAX_NAME_LENGTH] = {0};
    Context contexts[MAX_NAME_LENGTH];
    int context_index = -1;
    int parsing_contexts = 0;
    
    char *end = data + st.st_size;
    while (data < end) {
        char search_area[KEY_SEARCH_LIMIT + 1] = {0};
        strncpy(search_area, data, KEY_SEARCH_LIMIT);

        if (line_starts_with_alnum(search_area) && !strstr(search_area, "contexts:") && !strstr(search_area, "current-context:")) {
            parsing_contexts = 0;
        } else if (strstr(search_area, "contexts:")) {
            parsing_contexts = 1;
        } else if (parsing_contexts && strstr(search_area, "- context:")) {
            context_index++;
            strncpy(contexts[context_index].namespace, "default", MAX_NAME_LENGTH - 1);
        } else if (parsing_contexts && strstr(search_area, "name:")) {
            sscanf(data, " name: %63s", contexts[context_index].name);
        } else if (parsing_contexts && strstr(search_area, "namespace:")) {
            sscanf(data, " namespace: %63s", contexts[context_index].namespace);
        } else if (strstr(search_area, "current-context:")) {
            sscanf(data, "current-context: %63s", current_context);
        }
        
        while (*data != '\n' && data < end) {
            data++;
        }
        data++; // move past newline
    }

    munmap(data, st.st_size);

    // Find the namespace for the current context
    for (int i = 0; i <= context_index; i++) {
        if (strcmp(contexts[i].name, current_context) == 0) {
            printf("[ctx:%s/ns:%s]", current_context, contexts[i].namespace);
            break;
        }
    }

    return 0;
}

