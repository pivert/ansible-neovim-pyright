# Ensure 256 colors
export TERM=screen-256color

# Check if 'tput setaf 1' command works, which indicates that
# terminal color capabilities are supported.
if tput setaf 1 > /dev/null 2>&1; then
    # Reset terminal formatting to default
    tput sgr0

    # Check if the terminal supports at least 256 colors
    if [ "$(tput colors)" -ge 256 ] 2>/dev/null; then
        # Set colors using 256-color codes for Solarized color scheme
        BASE03=$(tput setaf 234)
        BASE02=$(tput setaf 235)
        BASE01=$(tput setaf 240)
        BASE00=$(tput setaf 241)
        BASE0=$(tput setaf 244)
        BASE1=$(tput setaf 245)
        BASE2=$(tput setaf 254)
        BASE3=$(tput setaf 230)
        YELLOW=$(tput setaf 136)
        ORANGE=$(tput setaf 166)
        RED=$(tput setaf 160)
        MAGENTA=$(tput setaf 125)
        VIOLET=$(tput setaf 61)
        BLUE=$(tput setaf 33)
        CYAN=$(tput setaf 37)
        GREEN=$(tput setaf 64)
    else
        # Fallback to basic color codes if 256 colors are not supported
        BASE03=$(tput setaf 8)
        BASE02=$(tput setaf 0)
        BASE01=$(tput setaf 10)
        BASE00=$(tput setaf 11)
        BASE0=$(tput setaf 12)
        BASE1=$(tput setaf 14)
        BASE2=$(tput setaf 7)
        BASE3=$(tput setaf 15)
        YELLOW=$(tput setaf 3)
        ORANGE=$(tput setaf 9)
        RED=$(tput setaf 1)
        MAGENTA=$(tput setaf 5)
        VIOLET=$(tput setaf 13)
        BLUE=$(tput setaf 4)
        CYAN=$(tput setaf 6)
        GREEN=$(tput setaf 2)
    fi

    # Set bold and reset formatting commands
    BOLD=$(tput bold)
    RESET=$(tput sgr0)
else
    # If 'tput' is not available, fall back to ANSI escape codes
    # These might not match the Solarized color scheme exactly
    MAGENTA="\033[1;31m"
    ORANGE="\033[1;33m"
    GREEN="\033[1;32m"
    PURPLE="\033[1;35m"
    WHITE="\033[1;37m"

    # Set bold and reset formatting commands to empty and default reset
    BOLD=""
    RESET="\033[m"
fi
