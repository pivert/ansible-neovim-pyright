######################################################################
# seashell additional config                                  #
######################################################################

# This first part is for all shells
export LS_OPTIONS='--color=auto'
export PSX='[\e[35m\t \e[31m\h\e[92m \w\e[0m]$ '
export KUBECONFIG=~/.kube/config

# Handy aliases
alias gcam='git commit -am'
alias gp='git push'
alias gco='git checkout'
alias gb='git branch'
alias ssha='eval $(ssh-agent -s) && ssh-add'
alias tmux='tmux -u -2'

# Some more aliases to avoid making mistakes:
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Nvim
alias view='nvim -R'
alias ex='nvim -e'
alias vimdiff='nvim -d'

# Easy way to go back to workdir
alias cdwd='cd /workdir'

# Easy way to see the relevant configuration of the container
alias config='env | nvim -R \
    /README.md /init.sh /etc/profile.d/python-nvim-ide.sh $HOME/.config/nvim/init.vim -'

# Common aliases
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'

# Enable color support for ls
eval "$(dircolors -b)"

# Ensure PATH are defined if needed
export PATH="$HOME/.krew/bin:$HOME/bin:$HOME/.local/bin:$PATH"

# Configure git with env
if [ ! -e $HOME/.gitconfig ]
then 
  if [ -f $HOME/.gitconfighost ]
  then
    cp -f $HOME/.gitconfighost $HOME/.gitconfig
  else
    [ -n "${GIT_EMAIL}" ] && git config --global user.email "${GIT_EMAIL}"
    [ -n "${GIT_NAME}" ] && git config --global user.name "${GIT_NAME}"
    git config --global merge.tool vimdiff
    git config --global merge.conflictstyle diff3
    git config --global mergetool.prompt false
  fi
fi 


# Configure ssh pubkey if provided
# Or mount your .ssh folder : 
# -v ~/.ssh:$HOME/.ssh
[ -n "${RSA_PUBKEY}" ] && echo "${RSA_PUBKEY}" >> $HOME/.ssh/id_rsa.pub

cat /etc/issue
cat /etc/motd

cd /workdir

# Handy alias to pipe though when log contains color codes
alias nocolors="sed 's/\x1b[[0-9;]*m//g'"

if [ -n "$BASH_VERSION" -a -n "$PS1" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
  . "$HOME/.bashrc"
  fi
fi
