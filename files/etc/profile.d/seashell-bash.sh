# This is bash-only addons (should work with ash)
if [ -n "$BASH_VERSION" ] || [ "$SH" = "/bin/ash" ]; then

  # Default to vim mode 
  set -o vi
  # but port common readline emacs style shortcuts
  # https://www.reddit.com/r/bash/comments/r38oi/if_you_use_vimode_under_bash_and_you_miss_escdot/
  bind -m vi-command ".":insert-last-argument
  bind -m vi-insert "\C-l.":clear-screen
  bind -m vi-insert "\C-a.":beginning-of-line
  bind -m vi-insert "\C-e.":end-of-line
  bind -m vi-insert "\C-w.":backward-kill-word
  bind -m vi-insert "\C-k":kill-line
  bind -x '"\C-l": clear;'

  # Change the terminal title
  echo -ne "\033]30;$(hostname)\007"

  # Set prompt
  __ctx='`/usr/local/bin/get_ctx_ns $KUBECONFIG`'
  __git_branch='`git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`'
  PS1="\[$BASE01\]\t\[$BLUE\][\h]\[$GREEN\][\u]\[$VIOLET\]$__git_branch\[$ORANGE\]$__ctx \[$CYAN\]\w\n\[$GREEN\]\$\[$RESET\]"
else
  PS1=${USER}@$(hostname)': $'
fi
