# System-wide .bashrc file for interactive bash(1) shells.

# Keep history for the current user only
export HISTFILE=/workdir/.bash_history_seashell

# Aliases & completion
source /usr/local/bin/complete_alias

for al in $(sed -En 's/^alias ([^=]+)=.*/\1/p' /etc/profile.d/aliases_kubectl.sh)
do complete -F _complete_alias "${al}"; done;

# aws-cli
complete -C '/usr/local/bin/aws_completer' aws
# Also completion for the a alias
alias a='aws'
complete -C '/usr/local/bin/aws_completer' a

# mkdir & cd
alias mkdircd='function _mkdircd() { mkdir -p "$1" && cd "$1"; }; _mkdircd'

# Completion for flux
if [[ -x /usr/local/bin/flux ]]; then
    . <(/usr/local/bin/flux completion bash)
fi

# k9s
. <(k9s completion bash)

# krsh & krshn functions with completion (inspired from oc rsh)
function krsh() {
    POD_NAME="$1"

    if [ -z "$POD_NAME" ]; then
        echo "Usage: krsh <pod_name>"
        return 1
    fi

    # Attempt to execute the shell in order
    for SHELL in /bin/bash /bin/ash /bin/sh; do
        if kubectl exec -it "$POD_NAME" -- $SHELL -c "echo 'Using shell: $SHELL'" &> /dev/null; then
            echo "Executing $SHELL in pod '$POD_NAME'..."
            kubectl exec -it "$POD_NAME" -- $SHELL
            return 0
        fi
    done

    echo "No available shell found in pod '$POD_NAME' in current namespace."
    return 1
}
function krshn() {
    POD_NAME="$2"
    NAMESPACE="${1}"  # Use 'default' namespace if not specified

    if [ -z "$POD_NAME" ]; then
        echo "Usage: krshn <namespace> <pod_name>"
        return 1
    fi

    # Attempt to execute the shell in order
    for SHELL in /bin/bash /bin/ash /bin/sh; do
        if kubectl exec -it "$POD_NAME" -n "$NAMESPACE" -- $SHELL -c "echo 'Using shell: $SHELL'" &> /dev/null; then
            echo "Executing $SHELL in pod '$POD_NAME'..."
            kubectl exec -it "$POD_NAME" -n "$NAMESPACE" -- $SHELL
            return 0
        fi
    done

    echo "No available shell found in pod '$POD_NAME' in namespace '$NAMESPACE'"
    return 1
}
_krsh_complete() {
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    if [[ ${COMP_CWORD} -eq 1 ]]; then
        opts=$(kubectl get pods --no-headers -o custom-columns=":metadata.name")
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    fi
    return 0
}
complete -F _krsh_complete krsh
_krshn_complete() {
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"

    if [[ ${COMP_CWORD} -eq 1 ]]; then
        # Complete with namespace names
        opts=$(kubectl get namespaces --no-headers -o custom-columns=":metadata.name")
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    elif [[ ${COMP_CWORD} -eq 2 ]]; then
        # Complete with pod names in the specified namespace
        local namespace="${COMP_WORDS[1]}"
        opts=$(kubectl get pods -n ${namespace} --no-headers -o custom-columns=":metadata.name")
        COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    fi
    return 0
}
complete -F _krshn_complete krshn



# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Commented out, don't overwrite xterm -T "title" -n "icontitle" by default.
# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
#    ;;
#*)
#    ;;
#esac

# enable bash completion in interactive shells
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
        function command_not_found_handle {
                # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
                   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
                   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
                else
                   printf "%s: command not found\n" "$1" >&2
                   return 127
                fi
        }
fi
