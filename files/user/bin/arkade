#!/usr/bin/env bash
#
# Install Arkade (open-source Kubernetes “market-place”)
#
# GNU GPL
# François Delpierre
# 20231017


set -Eeuo pipefail

cat <<EOF
arkade - Open Source Marketplace For Developer Tools
arkade is how developers install the latest versions of their favourite tools and Kubernetes apps.

EOF

if ! ( [[ "$#" -eq 1 ]] && [[ "$1" == "--install" ]] )
then
    read -p "$(basename "${BASH_SOURCE[0]}") is not installed. Install it now ? (y/n) " answer

    if [[ "$answer" != "y" ]]; then
        echo "Aborting installation of ${BASH_SOURCE[0]}"
        exit 0
    fi
fi

echo "Installing..."
curl -sLS https://get.arkade.dev | sudo sh 
/usr/local/bin/arkade completion bash | sudo tee /etc/bash_completion.d/arkade
echo ""
echo "To get Bash completion"
echo "----------------------"
echo "source /etc/bash_completion.d/arkade"
echo ""

# Clean up by removing the temporary directory
cd -

# Update the BINARY variable
BINARY=/usr/local/bin/$(basename "${BASH_SOURCE[0]}")

# Remove the echo in front of the sudo to get this script replaced with the link to the newly installed software
sudo ln -sf ${BINARY} ${BASH_SOURCE[0]}
