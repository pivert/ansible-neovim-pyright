#!/bin/bash
#
# Install me script
#
# GNU GPL
# François Delpierre
# 20230616
#
# Since it might not be used all the time, the default Ansible Collection is
# compressed (30MB instead of 500MB)
#
# This command untars it:
sudo tar -Jxvf $(python -c 'import site; print(site.getsitepackages()[0])')/ansible_collections.tar.xz -C /
